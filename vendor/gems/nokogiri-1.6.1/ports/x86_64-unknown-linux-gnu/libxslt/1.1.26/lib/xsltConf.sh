#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/home/katrina/repos/nuvosoft/demo/vendor/gems/nokogiri-1.6.1/ports/x86_64-unknown-linux-gnu/libxslt/1.1.26/lib"
XSLT_LIBS="-lxslt  -L/home/katrina/repos/nuvosoft/demo/vendor/gems/nokogiri-1.6.1/ports/x86_64-unknown-linux-gnu/libxml2/2.8.0/lib -lxml2 -lz -lm -lm"
XSLT_INCLUDEDIR="-I/home/katrina/repos/nuvosoft/demo/vendor/gems/nokogiri-1.6.1/ports/x86_64-unknown-linux-gnu/libxslt/1.1.26/include"
MODULE_VERSION="xslt-1.1.26"

class User < ActiveRecord::Base
  has_one :person
  has_many :vehicles
  accepts_nested_attributes_for :person, :vehicles

  attr_accessible :email, :password, :password_confirmation, :person #,:crypted_password, :salt
  authenticates_with_sorcery!

  validates :password, length: { minimum: 3 }
  validates :password, confirmation: true
  validates :password_confirmation, presence: true

  validates :email, uniqueness: true
end

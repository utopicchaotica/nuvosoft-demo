class Vehicle < ActiveRecord::Base
  belongs_to :user
  attr_accessible :colour, :license_plate, :make, :model, :user_id, :year
  validate :license_plate, :presence => true
  validate :make, :presence => true
  # validate :year, :length => {:exact => 4}
end

class Person < ActiveRecord::Base
  belongs_to :user
  attr_accessible :city, :first_name, :last_name, :postal_code, :province, :street_address, :telephone, :user_id
  validates :province, :format => { :inclusion => %w(BC AB SK MB ON QC NB NL NS NT YT NU PE) }
  canadian_postal_code = /^[A-CEGHJK-NPR-TVXY][0-9][A-CEGHJK-NPR-TV-Z](\s)?[0-9][A-CEGHJK-NPR-TV-Z][0-9]$/
  validates :postal_code, :format => {:with => canadian_postal_code}
  validates :phone, :length => {exact: 10}
end

require 'spec_helper'

describe "people/edit" do
  before(:each) do
    @person = assign(:person, stub_model(Person,
      :first_name => "MyString",
      :last_name => "MyString",
      :street_address => "MyString",
      :city => "MyString",
      :province => "MyString",
      :postal_code => "MyString",
      :telephone => 1,
      :user_id => 1
    ))
  end

  it "renders the edit person form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => people_path(@person), :method => "post" do
      assert_select "input#person_first_name", :name => "person[first_name]"
      assert_select "input#person_last_name", :name => "person[last_name]"
      assert_select "input#person_street_address", :name => "person[street_address]"
      assert_select "input#person_city", :name => "person[city]"
      assert_select "input#person_province", :name => "person[province]"
      assert_select "input#person_postal_code", :name => "person[postal_code]"
      assert_select "input#person_telephone", :name => "person[telephone]"
      assert_select "input#person_user_id", :name => "person[user_id]"
    end
  end
end

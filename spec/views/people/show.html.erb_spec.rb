require 'spec_helper'

describe "people/show" do
  before(:each) do
    @person = assign(:person, stub_model(Person,
      :first_name => "First Name",
      :last_name => "Last Name",
      :street_address => "Street Address",
      :city => "City",
      :province => "Province",
      :postal_code => "Postal Code",
      :telephone => 1,
      :user_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/First Name/)
    rendered.should match(/Last Name/)
    rendered.should match(/Street Address/)
    rendered.should match(/City/)
    rendered.should match(/Province/)
    rendered.should match(/Postal Code/)
    rendered.should match(/1/)
    rendered.should match(/2/)
  end
end

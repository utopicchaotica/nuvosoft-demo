require 'spec_helper'

describe "vehicles/show" do
  before(:each) do
    @vehicle = assign(:vehicle, stub_model(Vehicle,
      :license_plate => "License Plate",
      :colour => "Colour",
      :make => "Make",
      :model => "Model",
      :year => 1,
      :user_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/License Plate/)
    rendered.should match(/Colour/)
    rendered.should match(/Make/)
    rendered.should match(/Model/)
    rendered.should match(/1/)
    rendered.should match(/2/)
  end
end

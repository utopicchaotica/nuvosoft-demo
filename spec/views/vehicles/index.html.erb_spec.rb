require 'spec_helper'

describe "vehicles/index" do
  before(:each) do
    assign(:vehicles, [
      stub_model(Vehicle,
        :license_plate => "License Plate",
        :colour => "Colour",
        :make => "Make",
        :model => "Model",
        :year => 1,
        :user_id => 2
      ),
      stub_model(Vehicle,
        :license_plate => "License Plate",
        :colour => "Colour",
        :make => "Make",
        :model => "Model",
        :year => 1,
        :user_id => 2
      )
    ])
  end

  it "renders a list of vehicles" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "License Plate".to_s, :count => 2
    assert_select "tr>td", :text => "Colour".to_s, :count => 2
    assert_select "tr>td", :text => "Make".to_s, :count => 2
    assert_select "tr>td", :text => "Model".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
